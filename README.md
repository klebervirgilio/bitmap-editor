# Bitmap Editor

Produce a Ruby 2.4 program that simulates a basic bitmap editor. Bitmaps are represented as an M x N matrix of pixels with each element representing a colour.

## Running
1. Ensure to have `Ruby 2.4.x or above`
2. `bundle`
3. `>bin/bitmap_editor examples/show.txt`
4. run `rspec` for test

## Approach

* I used the Command Pattern to represent the available commands
* In order to parser the input, the parser can be easly configured to support all kinds of input
* App has a configuration class to support the bitmap editor settings

## Supported Commands
 | Command    | Description |
 |----------  |-------------|
 |I M N       |Create a new M x N image with all pixels coloured white (O).|
 |C           |Colours the pixel (X,Y) with colour C|
 |L X Y C     |Colours the pixel (X,Y) with colour C.|
 |V X Y1 Y2 C |Draw a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).|
 |H X1 X2 Y C |Draw a horizontal segment of colour C in row Y between columns X1 and X2 (inclusive).|
 |S           |Show the contents of the current image|

 ## Example

 `>bin/bitmap_editor examples/show.txt`

 * Input file
 ```
 I 5 6
 L 1 3 A
 V 2 3 6 W
 H 3 5 2 Z
 S
 ```

 * Expected Output
```
OOOOO
OOZZZ
AWOOO
OWOOO
OWOOO
OWOOO
```

This README is custom version of: https://raw.githubusercontent.com/lexiht/bitmap_editor/master/README.md
