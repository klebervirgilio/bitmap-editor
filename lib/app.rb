# frozen_string_literal: true

# App is the brigde between commands and the bitmap,
# all changes in the bitmap should be made via app using
# a command.
class App
  attr_accessor :bitmap, :config

  # Holds app configuration
  class AppConfig
    # TODO: Load defauls from a config file
    COLOUR = '0'.freeze
    COORDINATES_RANGE = (1..250).freeze

    attr_accessor :colour, :coordinates_range, :writer

    def colour
      @colour || COLOUR
    end

    def coordinates_range
      @coordinates_range || COORDINATES_RANGE
    end

    def writer
      @writer || STDOUT
    end
  end

  def initialize
    @config = AppConfig.new
  end

  def call(command, cmd_args)
    @bitmap = command.new(self, bitmap).call(cmd_args)
  end
end
