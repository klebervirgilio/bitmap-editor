# frozen_string_literal: true

# Interface whose defines a public API to deal with commands
class CommandParser
  attr_accessor :strategy

  def initialize(strategy)
    @strategy = strategy
  end

  def valid?(commands)
    strategy.valid?(commands)
  end

  def each_command(commands, &block)
    strategy.each_command(commands, &block)
  end
end

# Command Parser contract
class CommandParserStrategy
  def valid?(_commands)
    raise NotImplemented
  end

  def each_command(_commands)
    raise NotImplemented
  end
end

# Command Parser for a 'basic' File input. Other examples of file strategies are: CSVFileStrategy or JSONFileStrategy.
class BasicFileStrategy < CommandParserStrategy
  def valid?(file)
    File.exist?(file.to_s)
  end

  def each_command(file)
    File.open(file).each_line do |line|
      yield line.chomp.split(' ')
    end
  end
end
