# frozen_string_literal: true

# Represents the bitmap
class Bitmap
  attr_accessor :width, :height, :data
  def initialize(width, height)
    @width = width
    @height = height
  end
end
