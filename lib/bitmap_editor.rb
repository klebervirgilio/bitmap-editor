# frozen_string_literal: true

# BitmapEditor: is entrypoint class, it resolves the commands and use app algon with command
# to edit the bitmap.
class BitmapEditor
  attr_accessor :app, :command_parser

  def initialize(strategy: BasicFileStrategy.new)
    @command_parser = CommandParser.new(strategy)
    @app = App.new
  end

  def run(file)
    return app.config.writer.puts 'Invalid input, please make sure your input is valid.' unless command_parser.valid?(file)
    run_commands(file)
  end

  private

  def run_commands(commands)
    command_parser.each_command(commands) { |cmd| run_command(cmd) }
  end

  def run_command(command)
    cmd, *cmd_args = command
    app.call(command_for(cmd), cmd_args)
  rescue => e
    log_and_exit(e.message)
  end

  def command_for(cmd)
    case cmd
    when Commands::Create then Commands::Create
    when Commands::Clear then Commands::Clear
    when Commands::ColourPixel then Commands::ColourPixel
    when Commands::DrawVerticalLine then Commands::DrawVerticalLine
    when Commands::DrawHorizontalLine then Commands::DrawHorizontalLine
    when Commands::Show then Commands::Show
    else log_and_exit("unrecognised command: #{cmd}")
    end
  end

  def log_and_exit(msg)
    app.config.writer.puts msg
    exit(1)
  end
end
