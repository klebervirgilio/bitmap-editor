module Commands
  # Draw a vertical segment of colour C in column X between rows Y1 and Y2 (inclusive).
  class DrawVerticalLine < Command
    COMMAND_REGEXP = /^V/i
    include LineDrawalbe

    def call(args)
      x, y1, y2, colour = normalize_args(args)

      raise StandardError, 'Bitmap has not been correctly initialized' unless bitmap_valid?
      raise ArgumentError, 'Coordinates out of bounds ' unless valid_bounds?(x, y1, y2)

      cells(y1, y2).each { |y| colour_pixel(x, y, colour) }
      bitmap
    end

    def valid_bounds?(x, y1, y2)
      super(x, y1) && super(x, y2)
    end

    def cells(x1, x2)
      (x1..x2)
    end

    def colour_pixel(*args)
      ColourPixel.new(app, bitmap).call(args)
    end
  end
end
