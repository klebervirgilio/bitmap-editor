# frozen_string_literal: true

module Commands
  # Matches command line to command
  module CommandLineMatchable
    def ===(line)
      self::COMMAND_REGEXP.match?(line)
    end
  end

  # Some bitmap validations
  module BitmapValidatable
    def valid_bounds?(x, y)
      start_at = app.config.coordinates_range.begin
      (start_at..bitmap.width).cover?(x) && (start_at..bitmap.height).cover?(y)
    end

    def bitmap_valid?
      bitmap && bitmap.width && bitmap.height
    end
  end

  # Line drawable utils
  module LineDrawalbe
    def normalize_args(args)
      *ints, string = args
      [*Types.ensure_int(*ints), string]
    end
  end

  # type coerson utils
  module Types
    module_function

    def ensure_int(*args)
      args.map(&:to_i)
    end
  end
end
