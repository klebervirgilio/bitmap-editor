module Commands
  # Draw a horizontal segment of colour C in row Y between columns X1 and X2 (inclusive).
  class DrawHorizontalLine < Command
    COMMAND_REGEXP = /^H/i
    include LineDrawalbe

    def call(args)
      x1, x2, y, colour = normalize_args(args)

      raise StandardError, 'Bitmap has not been correctly initialized' unless bitmap_valid?
      raise ArgumentError, 'Coordinates out of bounds ' unless valid_bounds?(x1, x2, y)

      cells(x1, x2).each { |x| colour_pixel(x, y, colour) }
      bitmap
    end

    def cells(x1, x2)
      (x1..x2)
    end

    def colour_pixel(*args)
      ColourPixel.new(app, bitmap).call(args)
    end

    def valid_bounds?(x1, x2, y)
      super(x1, y) && super(x2, y)
    end
  end
end
