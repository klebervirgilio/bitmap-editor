# frozen_string_literal: true

module Commands
  # Show the contents of the current image
  class Show < Command
    COMMAND_REGEXP = /^S/i

    def call(*)
      show bitmap.data.map(&:join).join("\n")
    end

    def show(bitmap_string)
      app.config.writer.puts(bitmap_string)
    end
  end
end
