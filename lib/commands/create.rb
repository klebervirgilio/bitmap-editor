module Commands
  # Create a new M x N image with all pixels coloured white (O)
  class Create < Command
    COMMAND_REGEXP = /^I/i

    def call(args)
      width, height = Types.ensure_int(*args)
      raise ArgumentError, "Either width: #{width} or height: #{height} are invalid" unless valid_coordinates?(width, height)

      Bitmap.new(width, height).tap { |bm|
        bm.data = build_bitmap_data(width, height, app.config.colour)
      }
    end

    def valid_coordinates?(*coordinates)
      coordinates.all? { |coordinate| app.config.coordinates_range.cover?(coordinate) }
    end
  end
end
