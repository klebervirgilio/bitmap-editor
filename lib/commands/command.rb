# frozen_string_literal: true

module Commands
  # Command is base class for commands
  class Command
    extend CommandLineMatchable
    include BitmapValidatable

    attr_accessor :bitmap, :app

    def initialize(app, bitmap)
      @app = app
      @bitmap = bitmap
    end

    def build_bitmap_data(width, height, colour)
      Array.new(height) { Array.new(width, colour) }
    end
  end
end
