# frozen_string_literal: true

module Commands
  # Clears the table, setting all pixels to white (O)
  class Clear < Command
    COMMAND_REGEXP = /^C/i

    def call(*)
      raise StandardError, 'Bitmap has not been correctly initialized' unless bitmap_valid?

      bitmap.tap { |bm| bm.data = build_bitmap_data(bm.width, bm.height, app.config.colour) }
    end
  end
end
