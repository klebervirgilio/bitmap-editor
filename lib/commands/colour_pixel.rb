module Commands
  # Colours the pixel (X,Y) with colour C
  class ColourPixel < Command
    COMMAND_REGEXP = /^L/i

    def call(args)
      x, y, colour = args
      x, y = Types.ensure_int(x, y)

      raise StandardError, 'Bitmap has not been correctly initialized' unless bitmap_valid?
      raise ArgumentError, 'Coordinates out of bounds ' unless valid_bounds?(x, y)

      bitmap.data[y.pred][x.pred] = colour
      bitmap
    end
  end
end
