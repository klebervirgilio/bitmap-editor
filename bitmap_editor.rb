# frozen_string_literal: true

require './lib/app'
require './lib/bitmap'

require './lib/commands/commands'

require './lib/commands/command'
require './lib/commands/create'
require './lib/commands/colour_pixel'
require './lib/commands/clear'
require './lib/commands/show'
require './lib/commands/draw_horizontal_line'
require './lib/commands/draw_vertical_line'

require './lib/command_parser'

require './lib/bitmap_editor'
