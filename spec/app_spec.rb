require_relative '../lib/app'

describe App do
  describe '.call' do
    it 'changes bitmap calling a command' do
      app = described_class.new.tap { |a| a.bitmap = 'Bitmap' }
      cmd_class = double
      cmd_instance = double

      args = ['C', 1, 2, 3]

      expect(cmd_class).to receive(:new).with(app, 'Bitmap').and_return(cmd_instance)
      expect(cmd_instance).to receive(:call).with(args).and_return('Bitmap Changed')

      app.call(cmd_class, args)
      expect(app.bitmap).to eq('Bitmap Changed')
    end
  end
end
