# frozen_string_literal: true

require 'command_parser'

describe CommandParser do
  describe 'exposes command parser API' do
    subject { described_class.new(double) }
    specify { is_expected.to be_respond_to(:valid?) }
    specify { is_expected.to be_respond_to(:each_command) }
  end
end

describe BasicFileStrategy do
  describe '.valid?' do
    context 'when file exists' do
      it 'returns ture' do
        file = File.expand_path('.', __FILE__)
        expect(BasicFileStrategy.new.valid?(file)).to be true
      end
    end
    context 'when file does not exist' do
      it 'returns false' do
        file = '.x'
        expect(BasicFileStrategy.new.valid?(file)).to be false
      end
    end
  end

  describe '.each_command' do
    # FIXME: this test is too fragile and slow
    it 'yields each line of the file as an array' do
      file = File.expand_path('../fixtures/input', __FILE__)
      expect { |b| BasicFileStrategy.new.each_command(file, &b) }.to yield_successive_args(%w[A 1 2], %w[B 3 4])
    end
  end
end
