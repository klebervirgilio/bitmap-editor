# frozen_string_literal: true

require 'app'
require 'bitmap'
require 'commands/commands'
require 'commands/command'
require 'commands/create'
require 'commands/colour_pixel'
require 'commands/clear'
require 'commands/show'
require 'commands/draw_horizontal_line'
require 'commands/draw_vertical_line'

describe Commands::Create do
  describe '.call' do
    context 'when args are valid' do
      it 'returns a new bitmap' do
        expect(Commands::Create.new(App.new, nil).call([2, 2])).to be_a(Bitmap)
      end
    end
    context 'when args are invalid' do
      it 'raises an exception' do
        expect { Commands::Create.new(App.new, nil).call(['x', '']) }.to raise_error(ArgumentError)
      end
    end
  end
end

describe Commands::Clear do
  describe '.call' do
    context 'when args are valid' do
      it 'returns a new bitmap' do
        app = App.new
        bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
        Commands::Clear.new(app, bitmap).call
        expect(bitmap.data).to eq([[app.config.colour]])
      end
    end
    context 'when args are invalid' do
      it 'raises an exception' do
        expect { Commands::Clear.new(App.new, double(width: nil)).call }.to raise_error(StandardError)
      end
    end
  end
end

describe Commands::ColourPixel do
  describe '.call' do
    context 'when args are valid' do
      it 'returns a new bitmap' do
        app = App.new
        bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
        bitmap = Commands::ColourPixel.new(app, bitmap).call([1, 1, 'red'])
        expect(bitmap.data).to eq([['red']])
      end
    end
    context 'when args are invalid' do
      context 'not number ones' do
        it 'raises an exception' do
          bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
          expect { Commands::Create.new(App.new, bitmap).call('x', '') }.to raise_error(ArgumentError)
        end
      end
      context 'out of bounds' do
        it 'raises an exception' do
          bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
          expect { Commands::Create.new(App.new, bitmap).call(1_000, 20_000) }.to raise_error(ArgumentError)
        end
      end
    end
  end
end

describe Commands::DrawVerticalLine do
  describe '.call' do
    context 'when args are valid' do
      it 'returns a new bitmap' do
        app = App.new
        bitmap = Bitmap.new(5, 6).tap do |b|
          b.data = [
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.']
          ]
        end
        bitmap = Commands::DrawVerticalLine.new(app, bitmap).call([2, 3, 6, 'W'])
        expect(bitmap.data).to eq(
          [
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', 'W', '.', '.', '.'],
            ['.', 'W', '.', '.', '.'],
            ['.', 'W', '.', '.', '.'],
            ['.', 'W', '.', '.', '.']
          ]
        )
      end
    end
    context 'when args are invalid' do
      context 'out of bounds' do
        it 'raises an exception' do
          bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
          expect { Commands::DrawVerticalLine.new(App.new, bitmap).call([1, 1, 6, 'W']) }.to raise_error(ArgumentError)
        end
      end
    end
  end
end

describe Commands::DrawHorizontalLine do
  describe '.call' do
    context 'when args are valid' do
      it 'returns a new bitmap' do
        app = App.new
        bitmap = Bitmap.new(5, 6).tap do |b|
          b.data = [
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.']
          ]
        end
        bitmap = Commands::DrawHorizontalLine.new(app, bitmap).call([3, 5, 2, 'Z'])
        expect(bitmap.data).to eq(
          [
            ['.', '.', '.', '.', '.'],
            ['.', '.', 'Z', 'Z', 'Z'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.'],
            ['.', '.', '.', '.', '.']
          ]
        )
      end
    end
    context 'when args are invalid' do
      context 'out of bounds' do
        it 'raises an exception' do
          bitmap = Bitmap.new(1, 1).tap { |b| b.data = [['F']] }
          expect { Commands::DrawHorizontalLine.new(App.new, bitmap).call([1, 1, 6, 'W']) }.to raise_error(ArgumentError)
        end
      end
    end
  end
end

describe Commands::Show do
  describe '.call' do
    it 'calls app\'s writer puts method passing a string' do
      app = App.new
      string = StringIO.new
      app.config.writer = string
      bitmap = Bitmap.new(1, 1).tap { |b| b.data = [%w[F 0], %w[0 0]] }

      Commands::Show.new(app, bitmap).call([1, 1, 'red'])

      expect(string.string).to eq("F0\n00\n")
    end
  end
end
