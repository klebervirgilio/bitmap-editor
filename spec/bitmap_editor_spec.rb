# frozen_string_literal: true

require 'bitmap_editor'
require 'command_parser'
require 'app'

# Command Parser for StringIO input
class StringIOStrategy < CommandParserStrategy
  def valid?(string)
    !string.string.empty?
  end

  def each_command(string)
    string.each_line do |line|
      yield line.chomp
    end
  end
end

describe BitmapEditor do
  describe '.run' do
    it 'runs commands' do
      bitmap_editor = BitmapEditor.new(strategy: StringIOStrategy.new)
      expect(bitmap_editor).to receive(:run_command).with('B').twice
      bitmap_editor.run(StringIO.new("B\nB"))
    end

    context 'when input is invalid' do
      it 'does not run the command' do
        bitmap_editor = BitmapEditor.new(strategy: StringIOStrategy.new)
        expect(bitmap_editor).not_to receive(:run_command)
        bitmap_editor.run(StringIO.new)
      end
    end
  end
end
